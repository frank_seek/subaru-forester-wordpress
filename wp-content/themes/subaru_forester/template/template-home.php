<?php 
 /**
 * Template Name: home
 */
get_header();
?>
<section class="height-f section section-relative" id="section1">
	<div class="background-f">
		<div class="home-carousel">
			<?php
				$slider = get_field('slider');
				if (!empty($slider)) {
					foreach ($slider as $item) {
						?>
						<div class="home-item" style="background-image: url('<?php echo $item['imagen'];?>');"></div>
						<?php
					}
				}
			?>
		</div>
	</div>
	<div class="title-f">
		<div class="image-f">
			<img src="<?php the_field('title_imagen');?>">
			<h1><?php the_field('title_text');?></h1>
		</div>
	</div>
	<div class="footer-f">
		<div class="grid-footer-f">
			<div class="footer-left">
				<a href="#que-es" class="link-bottom modalJs"><?php the_field('text_quees');?></a>
			</div>
			<div class="footer-mid">
				<div class="button-container">
					<a href="#section2" class="btn scroll"><?php the_field('text_test');?></a>
					<a href="#section3" class="btn scroll"><?php the_field('text_allnew');?></a>
				</div>
			</div>
			<div class="footer-right">
				<a href="#video" class="link-bottom modalJs"><?php the_field('text_video');?></a>
			</div>
		</div>
	</div>
</section>
<section class="height-f section section-relative" id="section2" style="background: url('<?php the_field("background-section2");?>');">
	<div class="grid-align-center">
		<div class="container-x">
			<div class="title-x">
				<h2><?php the_field('title_section2'); ?></h2>
			</div>
			<div class="content-bajada-x">
				<ul id="title-question"></h3>
			</div>
			<div class="core-question">
				<div class="grid-core-question">
					<div class="box-question">
						<span class="box-title">- indorante</span>
						<div class="box-icon">
							<i></i>
						</div>
						<div class="box-link">
							<a href="javascript:void(0)" class="btn btnWhite" id="btnNo">NO</a>
						</div>
					</div>
					<div class="box-line-core">
						<div class="box-line">
							<div class="box-line-left" id="line-left"></div>
							<div class="box-line-icon moveDup">
								<i></i>
							</div>
							<div class="box-line-right" id="line-right"></div>
						</div>
						<div class="checkIn">
							<a href="javascript:void(0)" class="btn btnWhite btnBlueCore" id="checkIn" data=""></a>
						</div>
					</div>
					<div class="box-question">
						<span class="box-title">+ indorante</span>
						<div class="box-icon">
							<i></i>
						</div>
						<div class="box-link">
							<a href="javascript:void(0)" class="btn btnWhite" id="btnSi">Si</a>
						</div>
					</div>
				</div>
			</div>
			<div class="core-array-images">
				<ul id="icons-question">					
				</ul>
				<span class="background-core">
						<i></i>
					</span>
			</div>
		</div>
	</div>
</section>
<section class="section section-relative" id="section3">
	<div class="minHeight backgroundBlue section-relative">
		<div class="title-img-section3">
			<img src="<?php the_field('imagen-section3'); ?>">
			<h2><?php the_field('title-section3'); ?></h2>
		</div>
		<div class="abs-box-cont">
			<a href="javascript:void(0)" class="left-abs">
				<i></i>
				<span id="text-left-i"></span>
			</a>
			<a href="javascript:void(0)" class="right-abs">
				<span id="text-right-i"></span>
				<i></i>
			</a>
		</div>
	</div>
	<div class="backgroundWhite section-relative">
		<div class="container">
			<div class="model-carousel">
				<?php 
					$model = get_field('model');
					$a = 0;
					if (!empty($model)) {
						foreach ($model as $item) {
							?>
							<div class="model-item">
								<div class="model-img">
									<img src="<?php echo $item['imagen']; ?>">
								</div>
								<div class="model-data-line">
									<div class="grid-title-m">
										<div class="data-simple-title">
											<h3><?php echo $item['title']; ?></h3>
										</div>
										<div class="data-simple-list">
											<ul>
												<?php
													$lists = $item['list'];
													if (!empty($lists)){
														foreach ($lists as $list) {
															if ($list['check']) {
															?>
															<li>
																<div class="title">
																	<?php echo $list['title'];?>
																</div>
																<div class="data">
																	<?php echo $list['data'];?>
																</div>
															</li>
															<?php
															}
														}
													}
												?>
											</ul>
										</div>
										<div class="data-simple-link">
											<a href="#model-<?php echo $a;?>" class="modalJs link-left">Ver más especificaciones</a>
										</div>
									</div>
									<div class="grid-price-m">
										<div class="content-top sli-dolares">
											<div class="top-t">Desde</div>
											<div class="data-t">
												<span class="int-value">$</span>
												<span class="data-value">
													<?php echo $item['dolares']; ?>
												</span>
											</div>
										</div>
										<div class="content-top sli-soles">
											<div class="data-t">
												<span class="int-or">o</span>
												<span class="int-value">S/</span>
												<span class="data-value">
													<?php echo $item['soles']; ?>
												</span>
											</div>
											<div class="top-t">precio incluye IGV</div>
										</div>
									</div>
								</div>
								<div class="titleJs" data="<?php echo $item['title']; ?>"></div>
								<?php
									$showrooms = $item['showrooms'];
									if (!empty($showrooms)){
										foreach ($showrooms as $room) {
											?>
											<div class="showroomJs" data="<?php echo $room['title']; ?>" data-correo="<?php echo $room['correo']; ?>"></div>
											<?php
										}
									}
								?>
							</div>
							<?php
						$a++;
						}
					}
				?>
			</div>
			<div class="items-model-carousel">
				<?php 
					$model = get_field('model');
					$b = 0;
					if (!empty($model)) {
						foreach ($model as $item) {
							?>
							<div class="items-model-item changeItemsJs" data="<?php echo $b;?>">
								<img src="<?php echo $item['imagen']; ?>">
								<h3><?php echo $item['simple-title']; ?></h3>
							</div>
							<?php
						$b++;
						}
					}
				?>
			</div>
			<div class="link-content-model">
				<div class="terms-conditions">
					<div class="terms-span">
						<i></i>
						<span>He leído y acepto autorización de datos personales y el tratamiento de mis datos según lo especificado en las mismas. </span>
					</div>
					<div class="terms-link">
						<a href="#terms" class="modalJs">Leer Términos y Condiciones.</a>
					</div>
				</div>
				<a href="#form" class="modalJs-especial btn btnBlue">Cotizar All New Forester</a>
			</div>
		</div>
	</div>
</section>
<section class="section section-relative" id="section4">
	<div class="container">
		<div class="title-x">
			<h2><?php the_field('title-section4');?></h2>
		</div>
		<div class="cards-grid">
			<?php
				$cards = get_field('otrasformas');
				$aa = 0;
				if (!empty($cards)) {
					foreach ($cards as $card) {
						?>
						<div class="card-item">
							<div class="card-item-image">
								<img src="<?php echo $card['imagen']; ?>">
							</div>
							<div class="card-item-content">
								<h3><?php echo $card['title']; ?></h3>
								<div class="intent-content">
									<?php echo $card['description']; ?>
								</div>
							</div>
							<div class="button-fot">
								<a href="#card-<?php echo $aa; ?>" class="modalJs circlebtn"><i></i></a>
							</div>
						</div>
						<?php
						$aa++;
					}
				} 
			?>
		</div>
	</div>
</section>

<!--modales-->

<div id="que-es" class="creepy-modal">
	<div class="content-creepy-modal">
		<div class="close-creepy-modal"></div>
		<div class="body-content-creepy-modal center-text">
			<?php the_field('content-quees-indorancia');?>
		</div>
	</div>
</div>
<div id="video" class="creepy-modal">
	<div class="contentfull-creepy-modal">
		<div class="close-creepy-modal"></div>
		<div class="bodyfull-content-creepy-modal video">
			<iframe width="100%" height="100%" src="<?php the_field('video');?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	</div>
</div>
<div id="result-fail" class="creepy-modal">
	<div class="content-creepy-modal">
		<div class="close-creepy-modal"></div>
		<div class="body-content-creepy-modal center-text">
			<?php the_field('indorancia-result');?>
			<div class="body-footer-modal">
				<p>Combate la indoorancia con</p>
				<a href="#section3" class="btn scroll2">All New Forester 2019</a>
			</div>
		</div>
	</div>
</div>
<div id="terms" class="creepy-modal">
	<div class="content-creepy-modal">
		<div class="close-creepy-modal"></div>
		<div class="body-content-creepy-modal simple-desing-text">
			<?php the_field('terminos-full');?>
		</div>
	</div>
</div>
<div id="form" class="creepy-modal">
	<div class="content-creepy-modal">
		<div class="close-creepy-modal"></div>
		<div class="body-content-creepy-modal full-contact-template">
			<?php the_field('contacto-full');?>
		</div>
	</div>
</div>
<?php 
	$model = get_field('model');
	$c = 0;
	if (!empty($model)) {
		foreach ($model as $item) {
?>
	<div id="model-<?php echo $c;?>" class="creepy-modal">
		<div class="contentfull-creepy-modal">
			<div class="close-creepy-modal"></div>
			<div class="bodyfull-content-creepy-modal modal-model">
				<div class="container">
					<div class="title-core">
						<h2><?php echo $item['title']; ?></h2>
						<p><?php echo $item['simple-title']; ?></p>
					</div>
					<div class="img-core">
						<img src="<?php echo $item['second-image']; ?>" class="img-back">
						<img src="<?php echo $item['imagen']; ?>" class="img-front">
					</div>
					<div class="list-core">
						<ul>
							<?php
								$lists = $item['list'];
								$r = 0;
								if (!empty($lists)){
								foreach ($lists as $list) {
							?>
								<?php
									if ($r == 7) {
										echo '<li class="not-me-line"></li>';
									}
								?>
								<li class="<?php if ($r > 6) {echo 'horizontal-li'; } else {echo 'vertical-li'; }?>">
									<div class="title">
										<?php echo $list['title'];?>
									</div>
									<div class="data">
										<?php echo $list['data'];?>
									</div>
								</li>							
							<?php
							$r++;
							}
						}
						?>
						</ul>						
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
	$c++;
		}
	}
?>
<?php
	$cards = get_field('otrasformas');
	$aa = 0;
	if (!empty($cards)) {
		foreach ($cards as $card) {
?>
	<div id="card-<?php echo $aa;?>" class="creepy-modal">
		<div class="content-creepy-modal">
			<div class="close-creepy-modal"></div>
			<div class="body-content-creepy-modal">
				<div class="content-modal-card">
					<h3><?php echo $card['title']; ?></h3>
					<ul>
						<?php
							$lists = $card['lists'];
							foreach ($lists as $list) {
								?>
								<li>
									<img src="<?php echo $list['imagen']; ?>">
									<div class="text">
										<?php echo $list['text']; ?>
									</div>
								</li>
								<?php
							}
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php
	$aa++;
		}
	}
?>

<?php
get_footer();
?>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.js"></script>
<script type="text/javascript">
(function($) {
	function svg() {
		jQuery('#icons-question img').each(function(){
		    var $img = jQuery(this);
		    var imgID = $img.attr('id');
		    var imgClass = $img.attr('class');
		    var imgURL = $img.attr('src');

		    jQuery.get(imgURL, function(data) {
		        // Get the SVG tag, ignore the rest
		        var $svg = jQuery(data).find('svg');

		        // Add replaced image's ID to the new SVG
		        if(typeof imgID !== 'undefined') {
		            $svg = $svg.attr('id', imgID);
		        }
		        // Add replaced image's classes to the new SVG
		        if(typeof imgClass !== 'undefined') {
		            $svg = $svg.attr('class', imgClass+' replaced-svg');
		        }

		        // Remove any invalid XML tags as per http://validator.w3.org
		        $svg = $svg.removeAttr('xmlns:a');

		        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
		        if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
		            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
		        }

		        // Replace image with new SVG
		        $img.replaceWith($svg);

		    }, 'xml');
		});
	}
	function modal(){
		$('.modalJs').on('click',function(e){
			e.preventDefault();
			var $this = $(this);
			var id = $this.attr('href');
			$(id).addClass('is-active');
			$('body').addClass('modal-active');
		});
		$('.close-creepy-modal').on('click',function(e){			
			$('.creepy-modal').removeClass('is-active');
			$('body').removeClass('modal-active');
		});
		$('.modalJs-especial').on('click',function(e){
			e.preventDefault();
			var $this = $(this);
			var id = $this.attr('href');
			$(id).addClass('is-active');
			var $item = $('.model-carousel .owl-item');
			for (var a=0;a<$item.length;a++){
				if ($item.eq(a).hasClass('active')){
					var $title = $item.eq(a).find('.titleJs');
					var title_c = $title.attr('data');
					$('#version').val(title_c);
					var $show = $item.eq(a).find('.showroomJs');
					$('#showrooms').html('<option value="Showrooms">Showrooms</option>');
					for (var b=0;b<$show.length;b++){						
						$('#showrooms').append('<option value="'+$show.eq(b).attr('data')+'" data="'+$show.eq(b).attr('data-correo')+'">'+$show.eq(b).attr('data')+'</option>');
					}
				}
			}
			$('body').addClass('modal-active');
		});
	}
	function nextQuestion(number){
		var $li = $('#icons-question').find('li');
		$li.removeClass('is-active');
		$li.eq(number).addClass('is-active');
		var $title = $('#title-question');
		$title.css('transform','translateX(-'+number*100+'%)');
		$('.background-core').css('transform','translateX('+number*100+'%)');
	}
	function getRandom(arr, n) {
	    var result = new Array(n),
	        len = arr.length,
	        taken = new Array(len);
	    if (n > len)
	        throw new RangeError("getRandom: more elements taken than available");
	    while (n--) {
	        var x = Math.floor(Math.random() * len);
	        result[n] = arr[x in taken ? taken[x] : x];
	        taken[x] = --len in taken ? taken[len] : len;
	    }
	    return result;
	}
	function calculateRespuesta(anwser_real,anwser_ideal){
		var sum = 0;
		for (var a=0;a<anwser_ideal.length;a++){
			if (anwser_real[a] != anwser_ideal[a]) {
				sum++;
			}
		}
		var res = sum*20;
		return res;
	}
	function capturenames(){
		var b=0;
		var c=0;
		var $item = $('.model-carousel .owl-item');
			for (var a=0;a<$item.length;a++){
				if ($item.eq(a).hasClass('active')){
					if (a == 0) {
						b = 1;
						c = $item.length-1;
					} else {
						b = a + 1;
						c = a - 1;
					}
				}
			}
		$('#text-left-i').html($item.eq(c).find('.titleJs').attr('data'));
		$('#text-right-i').html($item.eq(b).find('.titleJs').attr('data'));
	}
	function capturelist(){
		$('.changeItemsJs').removeClass('is-active');
		var $item = $('.model-carousel .owl-item');
			for (var a=0;a<$item.length;a++){
				if ($item.eq(a).hasClass('active')){
					$('.changeItemsJs').eq(a).addClass('is-active');
				}
			}
	}
	$(document).ready(function(){
		$(window).on('load',function(){
			/*$('.home-carousel').owlCarousel({
				dots:false,
				nav:false,
				loop:true,
				autoplay: true,			
				autoplayTimeout:15000,
				autoplayHoverPause:true,
				autoHeight: true,
				items:1
			});*/
			$('.model-carousel').owlCarousel({
				dots:true,
				nav:true,
				items:1,
			});			
			capturenames();
			capturelist();
			$('.model-carousel').on('changed.owl.carousel', function(event) {
			    capturenames();
				capturelist();
			});
			$('.left-abs').on('click',function(){
				$('.model-carousel .owl-prev').click();
				capturenames();
				capturelist();
			});
			$('.right-abs').on('click',function(){
				$('.model-carousel .owl-next').click();
				capturenames();
				capturelist();
			});
			$('.changeItemsJs').on('click',function(){
				var $this = $(this);
				var data = $this.attr('data');
				$('.changeItemsJs').removeClass('is-active');
				$this.addClass('is-active');
				$('.model-carousel').find('.owl-dot').eq(data).click();		
				capturenames();
				capturelist();	
			});
		}); 
		modal();
		var question = <?php echo json_encode(get_field('question')); ?>;
		var newQuestion = getRandom(question,5);
		var anwser_ideal = [];
		//console.log(newQuestion);
		for (var a=0;a<newQuestion.length;a++){
			$('#icons-question').append('<li><img src="'+newQuestion[a].imagen+'"></li>');
			$('#title-question').append('<li><h3>'+newQuestion[a].text+'</h3></li>');
			anwser_ideal.push(newQuestion[a].valor);
		}
		svg();
		$('#icons-question').find('li').eq(0).addClass('is-active');
		var questionNum = 0;
		var questionAnswer = 0;
		var tamLeft = $('#line-left').width()+13;


		$('#btnNo').on('click',function(){
			$('#btnNo').addClass('active');
			$('#checkIn').attr('data','no');
			$('.checkIn').show();
			$('.box-line-icon').css('transform','translateX(-'+tamLeft+'px)');
		});	
		$('#btnSi').on('click',function(){
			$('#btnSi').addClass('active');
			$('#checkIn').attr('data','si');
			$('.checkIn').show();
			$('.box-line-icon').css('transform','translateX('+tamLeft+'px)');
		});
		var anwser_real = [];	
		$('#checkIn').on('click',function(){
			$('#btnNo').removeClass('active');
			$('#btnSi').removeClass('active');
			var data = $('#checkIn').attr('data');
			if (data == 'no'){
				questionAnswer--;
			}
			if (data == 'si'){
				questionAnswer++;
			}
			$('.box-line-icon').css('transform','none');
			anwser_real.push(data);
			questionNum++;
			if (questionNum == 5){
				console.log(questionAnswer);
				$('#btnNo').addClass('not-active');
				$('#btnSi').addClass('not-active');
				questionNum=4;
				var valor = calculateRespuesta(anwser_real,anwser_ideal);
				console.log(valor);
				if (valor == 0) {
					window.location.href = "<?php echo site_url();?>/diploma";
				} else {
					$('#number').html(valor);
					$('#result-fail').addClass('is-active');
				}
			}			
			nextQuestion(questionNum);
			$('.checkIn').hide();
		});	
		$('.scroll').on('click', function(event){
		    event.preventDefault();
		    var $this = $(this);
		    var id = $this.attr('href');
		    $('html, body').stop().animate({scrollTop: $(id).offset().top}, 500);
		});
		$('.scroll2').on('click', function(event){
		    event.preventDefault();
		    var $this = $(this);
		    var id = $this.attr('href');
		    $('html, body').stop().animate({scrollTop: $(id).offset().top}, 500);
		    $('#result-fail').removeClass('is-active');
		});

		$('#showrooms').on('change',function(){
			var idx = this.selectedIndex;        
    		var mail = $('#showrooms option').eq(idx).attr('data');
    		$('#extracorreo').val(mail);
		});
	}); 
})( jQuery );
</script>