<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'seek_forester');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '^X@`BW7]Jc+ju=h3Q$^+zURfg:HGOn P*0C5;p2Fjj!EvV_@!Lp]&Da7cy+#H|s4');
define('SECURE_AUTH_KEY', 'x?GaKf(0Y>DP~7oA~Zaf[}@Whl3D3q~UipWZ3-:Zf!|aD}wp.s<@~+=Ub^fK85GT');
define('LOGGED_IN_KEY', '&3E/z?deAodyhQi;S~!(eG<W&Z}?Qhm2(X>UZmdn.3`FgvjIMpYD+mQKE+jzw6OU');
define('NONCE_KEY', '6Sf~b@`dxhqS: OYaN~kl@32*^&Knwts+XQ=Md>:T*<jTh&#92fYJg}lYwd@q9BO');
define('AUTH_SALT', '7wr:dHXB}-aI3cSaq+O,M_C&dA7SF8OlWS@!VLKcRj)V>U??A@s|W&7*TBc/OC8s');
define('SECURE_AUTH_SALT', 'FEIG0rzMarVVHsN5J}u6Wu*B$|,_kh0O<t3rk^*uNp;s:arZ~ CGE-Qp!R]K8gMO');
define('LOGGED_IN_SALT', 'Y2Po^8$,c<cIAryYS2e<G mb7Q|T:-KfC$:/!wHau`h1Y&)MFp|^_@@)v`[MGas9');
define('NONCE_SALT', '>/vI?F^I<uD#|#F^lyz9s1|Vu@=rCPI4ks|mP`+f N)xD^:C]Cr2-X,~i= mfcvI');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

